package com.tgl.homework.fileIO;

public class Employee {
	
	private int height;
	private int weight;
	private String englishName;
	private String chinessName;
	private String phone;
	private String email;
	private int id;
	private float bmi;
	public Employee(int height, int weight, String englishName, String chinessName, String phone, String email, int id, float bmi) {
		this.height = height;
		this.weight = weight;
		this.englishName = englishName;
		this.chinessName = chinessName;
		this.phone = phone;
		this.email = email;
		this.id = id;
		this.bmi = bmi;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public String getEnglishName() {
		return englishName;
	}
	
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	
	public String getChinessName() {
		return chinessName;
	}
	
	public void setChinessName(String chinessName) {
		this.chinessName = chinessName;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public float getBmi() {
		bmi = (float) weight / (float) Math.pow(((float)height/100), 2);
		return bmi;
	}

	public void setBmi(float bmi) {
		this.bmi = bmi;
	}
	
	@Override
	public String toString() {
		return "employee [height=" + height + ", weight=" + weight + ", englishName=" + englishName +", chinessName=" + chinessName + ", phone=" + phone + ", email=" + email + ", id=" + id + ", bmi=" + getBmi() + "]\n";
	}
	
}
