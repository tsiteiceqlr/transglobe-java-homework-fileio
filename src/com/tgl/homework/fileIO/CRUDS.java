package com.tgl.homework.fileIO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tgl.homework.fileIO.compartor.bmiComparator;
import com.tgl.homework.fileIO.compartor.heightComparator;
import com.tgl.homework.fileIO.compartor.weightComparator;

public class CRUDS {
	
	FileIO rf = new FileIO();
	List<Employee> list = rf.readFile();
	List<Employee> newList = new ArrayList<>();
	
	public List<Employee> search(Factor factor, String employee) {
//		if (!Factor.forName(factor)) {
//			return;
//		}
		switch (factor) {
		case ID:
			newList.add(list.get(Integer.parseInt(employee) - 1));
			break;

		case ENAME:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getEnglishName().equals(employee)) {
					newList.add(list.get(i));
				}
			}
			break;

		case CNAME:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getChinessName().equals(employee)) {
					newList.add(list.get(i));
				}
			}
			break;

		case PHONE:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getPhone().equals(employee)) {
					newList.add(list.get(i));
				}
			}
			break;

		case EMAIL:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getEmail().equals(employee)) {
					newList.add(list.get(i));
				}
			}
			break;

		default:
			System.out.println("請重新操作");
			break;
		}
		return newList;
	}

	public List<Employee> sort(Factor factor) {
//		if (!Factor.forName(factor)) {
//			return;
//		}
		Map<String, Integer> tm = new TreeMap<>();
		
		switch (factor) {
		case ID:
			for (int i = 0; i < list.size(); i++) {
				newList.add(list.get(i));
			}
			break;

		case HEIGHT:
			Collections.sort(list, new heightComparator());
			for (int i = 0; i < list.size(); i++) {
				newList.add(list.get(i));
			}
			break;

		case WEIGHT:
			Collections.sort(list, new weightComparator());
			for (int i = 0; i < list.size(); i++) {
				newList.add(list.get(i));
			}
			break;

		case ENAME:
			for (int i = 0; i < list.size(); i++) {
				tm.put(list.get(i).getEnglishName(), list.get(i).getId());
			}
			for (Map.Entry<String, Integer> entry : tm.entrySet()) {
				int value = entry.getValue();
				newList.add(list.get(value-1));
			}
			break;

		case PHONE:
			for (int i = 0; i < list.size(); i++) {
				tm.put(list.get(i).getPhone(), list.get(i).getId());
			}
			for (Map.Entry<String, Integer> entry : tm.entrySet()) {
				int value = entry.getValue();
				newList.add(list.get(value-1));
			}
			break;

		case EMAIL:
			for (int i = 0; i < list.size(); i++) {
				tm.put(list.get(i).getEmail(), list.get(i).getId());
			}
			for (Map.Entry<String, Integer> entry : tm.entrySet()) {
				int value = entry.getValue();
				newList.add(list.get(value-1));
			}
			break;

		case BMI:
			Collections.sort(list, new bmiComparator());
			for (int i = 0; i < list.size(); i++) {
				newList.add(list.get(i));
			}
			break;

		default:
			System.out.println("請重新操作");
			break;
		}
		return newList;
	}

	public long insert(int height, int weight, String englishName, String chinessName, String phone, String email) {
		String path = "D:\\Documents\\JAVA\\employee.txt";
		
		try (Writer fw = new FileWriter(path, true)) {
			fw.write("\n");
			fw.write(height + " ");
			fw.write(weight + " ");
			fw.write(englishName + " ");
			fw.write(chinessName + " ");
			fw.write(phone + " ");
			fw.write(email + " ");
			
			System.out.println("success...");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File f = new File(path);
		return f.length();
	}

	public List<Employee> delete(Factor factor, String employee) {
//		if (!Factor.forName(factor)) {
//			return;
//		}
		switch (factor) {
		case ID:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == Integer.parseInt(employee)) {
					continue;
				} else {
					newList.add(list.get(i));
				}
			}
			System.out.println("new size: " + newList.size());
			break;

		case ENAME:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getEnglishName().equals(employee)) {
					continue;
				} else {
					newList.add(list.get(i));
				}
			}
			System.out.println("new size: " + newList.size());
			break;

		case PHONE:
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getPhone().equals(employee)) {
					continue;
				} else {
					newList.add(list.get(i));
				}
			}
			System.out.println("new size: " + newList.size());
			break;

		default:
			System.out.println("請重新操作");
			break;
		}
		return newList;
	}
	
	public List<Employee> maxmin(Factor factor) {
//		if (!Factor.forName(factor)) {
//			return;
//		}
		switch (factor) {
		case HEIGHT:
			Collections.sort(list, new heightComparator());
			newList.add(list.get(0));
			newList.add(list.get(list.size()-1));
			break;

		case WEIGHT:
			Collections.sort(list, new weightComparator());
			newList.add(list.get(0));
			newList.add(list.get(list.size()-1));
			break;
			
		case BMI:
			Collections.sort(list, new bmiComparator());
			newList.add(list.get(0));
			newList.add(list.get(list.size()-1));
			break;

		default:
			System.out.println("請重新操作");
			break;
		}
		return newList;
	}
	
}
