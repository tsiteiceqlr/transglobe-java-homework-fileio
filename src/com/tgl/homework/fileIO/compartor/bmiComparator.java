package com.tgl.homework.fileIO.compartor;

import java.util.Comparator;

import com.tgl.homework.fileIO.Employee;

public class bmiComparator implements Comparator<Employee> {
	@Override
	public int compare(Employee b1, Employee b2) {
		if (b1.getBmi() == b2.getBmi())
			return 0;
		else if (b1.getBmi() > b2.getBmi())
			return 1;
		else
			return -1;
	}
}
