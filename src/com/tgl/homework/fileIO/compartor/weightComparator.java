package com.tgl.homework.fileIO.compartor;

import java.util.Comparator;

import com.tgl.homework.fileIO.Employee;

public class weightComparator implements Comparator<Employee> {
	@Override
	public int compare(Employee w1, Employee w2) {
		if (w1.getWeight() == w2.getWeight())
			return 0;
		else if (w1.getWeight() > w2.getWeight())
			return 1;
		else
			return -1;
	}
}
