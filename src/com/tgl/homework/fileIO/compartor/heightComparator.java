package com.tgl.homework.fileIO.compartor;

import java.util.Comparator;

import com.tgl.homework.fileIO.Employee;

public class heightComparator implements Comparator<Employee> {

	@Override
	public int compare(Employee h1, Employee h2) {
		if (h1.getHeight() == h2.getHeight())
			return 0;
		else if (h1.getHeight() > h2.getHeight())
			return 1;
		else
			return -1;
	}

}
