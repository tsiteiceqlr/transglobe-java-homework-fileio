package com.tgl.homework.fileIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileIO {

	public static void main(String[] args) {
		CRUDS method = new CRUDS();
		List<Employee> n;
//		List<Employee> n = method.search(Factor.ID, "5");
		
		n = method.sort(Factor.HEIGHT);
//		method.insert(174, 90, "Luke-Lan", "藍浚哲", "5566", "yt0128@transglobe.com.tw");
//		method.delete(Factor.PHONE, "6120");
//		method.maxmin(Factor.WEIGHT);
		System.out.println(n);
		System.out.println(n.size());

	}

	public List<Employee> readFile() {
		List<Employee> list = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader("D:\\Documents\\JAVA\\employee.txt"))) {
			String i;
			String[] array;
			int id = 1;
			while ((i = reader.readLine()) != null) {
					array = i.split(" ");
					list.add(new Employee(Integer.parseInt(array[0]), Integer.parseInt(array[1]), array[2], array[3],
							array[4], array[5], id, 0));
					id += 1;
				}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return list;
	}
	
}
